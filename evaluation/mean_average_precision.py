from functools import reduce
import matplotlib.pyplot as plt
import numpy as np


def precisions_at_rank(ratings, threshold):
    precisions = []
    number_of_relevant_docs = 0
    number_of_docs = 0

    for rating in ratings:
        number_of_docs += 1
        if rating >= threshold:
            number_of_relevant_docs += 1

        precision = number_of_relevant_docs / number_of_docs
        precisions.append(precision)

    return precisions


def average_precision(precisions, ratings, threshold):
    if len(precisions) == 0:
        return -1

    if reduce(lambda  x, y: x + y, ratings) == 0:
        return 0

    precisions_at_rank_with_relevant_doc = map(
        lambda pair: pair[0],
        filter(
            lambda pair: pair[1] >= threshold,
            zip(precisions, ratings)
        )
    )

    sum_precisions = reduce(lambda x, y: x + y, precisions_at_rank_with_relevant_doc)
    number_relevant_docs = len(list(filter(lambda rating: rating >= threshold, ratings)))
    return sum_precisions / number_relevant_docs


def average_precision_distribution(ratings_list, threshold, topics):
    precisions_list = list(map(lambda ratings: precisions_at_rank(ratings, threshold), ratings_list))
    avg_precisions = list(map(lambda pair: average_precision(pair[0], pair[1], threshold), zip(precisions_list, ratings_list)))
    mean = mean_average_precision(ratings_list, threshold)
    distribution = list(sorted(zip(avg_precisions, topics), key=lambda pair: -pair[0]))
    x = np.arange(len(topics))
    plt.bar(x, list(map(lambda pair: pair[0], distribution)))
    plt.xticks(x, list(map(lambda pair: pair[1], distribution)))
    plt.xlabel('topic ids')
    plt.ylabel('avg. precision')
    plt.axhline(y=mean, zorder=0, color='red')
    plt.show()


def mean_average_precision(ratings_list, threshold):
    precisions_list = list(map(lambda ratings: precisions_at_rank(ratings, threshold), ratings_list))
    avg_precisions = list(map(lambda pair: average_precision(pair[0], pair[1], threshold), zip(precisions_list, ratings_list)))
    avg_precisions = list(filter(lambda x: x >= 0, avg_precisions))
    return sum(avg_precisions) / len(avg_precisions)
