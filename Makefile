.PHONY: all
all: data/webapp.war

.PHONY: test
test:
	nosetests crawler/tests
	cd indexer && ant test

.PHONY: index
index: data/index/marker

.PHONY: scrape
scrape: data/db/db.sqlite

.PHONY: webapp
webapp: data/webapp.war

.PHONY: download
download: data/crawler/marker

.PHONY: deploy
deploy: data/webapp.war
	mkdir -p we
	rm -rf we/*
	cp data/webapp.war we/ROOT.war

data/crawler/marker: $(shell find crawler/*)
	cd crawler && python scraper.py --noparse $(SCRAPEARGS)
	touch data/crawler/marker

data/db/db.sqlite: data/crawler/marker $(shell find crawler/*)
	rm -f data/db/db.sqlite
	cd crawler && python scraper.py --nodl $(SCRAPEARGS)

indexer/dist/groupA.jar: $(shell find indexer/src) $(shell find indexer/nbproject)
	cd indexer && ant jar

data/index/marker: data/db/db.sqlite indexer/dist/groupA.jar
	rm -f data/index/*
	cd indexer && java -cp "$(CURDIR)/indexer/lib/*:$(CURDIR)/indexer/dist/*" groupa.GroupA build ../data/db/db.sqlite ../data/index $(INDEXERARGS)
	touch data/index/marker

data/webapp.war: data/index/marker data/db/db.sqlite $(shell find webapp/src) $(shell find webapp/nbproject) $(shell find webapp/web)
	rm -f webapp/web/WEB-INF/index/* && cp -r data/index/ webapp/web/WEB-INF
	rm -f webapp/web/WEB-INF/taxoIndex/* && cp -r data/taxoIndex/ webapp/web/WEB-INF
	rm -f webapp/web/htmlProtocol/* && cp -r data/htmlProtocol/ webapp/web
	cp data/db/db.sqlite webapp/web/WEB-INF/db.sqlite
	mkdir -p indexer/dist/lib && cp indexer/lib/* indexer/dist/lib
	cd webapp && ant "-Dj2ee.server.home=$(j2ee_server_home)" "-Dj2ee.platform.classpath=$(j2ee_platform_classpath)" "-Dlibs.CopyLibs.classpath=$(libs_CopyLibs_classpath)"
	cp webapp/dist/webapp.war data

.PHONY: clean
clean:
	rm -rf indexer/dist
	rm -rf indexer/build
	rm -rf webapp/dist
	rm -rf webapp/build
	rm -f webapp/web/WEB-INF/db.sqlite
	rm -rf webapp/web/WEB-INF/index
