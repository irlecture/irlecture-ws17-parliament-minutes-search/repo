# coding: utf-8
from _functools import reduce

EMOTIONS = [{'type': 'Heiterkeit', 'terms': ['heiterkeit'], 'frequency': 5412, 'emoticon': '&#128522;'},
            {'type': 'Lachen', 'terms': ['lachen'], 'frequency': 3360, 'emoticon': '&#128516;'},
            {'type': 'Beifall', 'terms': ['beifall'], 'frequency': 179827, 'emoticon': '&#128079;'},
            {'type': 'Zustimmung', 'terms': ['zustimmung'], 'frequency': 85, 'emoticon': '&#128077;'},
            {'type': 'Widerspruch', 'terms': ['widerspruch', 'intervention'], 'frequency': 2156, 'emoticon': '&#128078;'},
            {'type': 'Unruhe', 'terms': ['unruhe'], 'frequency': 226},
            {'type': 'Frage', 'terms': ['zwischenfrage', 'nachfrage'], 'frequency': 399},
            {'type': 'Zuruf', 'terms': ['zuruf'], 'frequency': 5221},
            {'type': 'Gegenruf', 'terms': ['gegenruf'], 'frequency': 38},
            {'type': 'Gratulation', 'terms': ['glückwünsch', 'gratul'], 'frequency': 23}]


def parse_emotion(type, speaker_fingerprint, text):
    if type != 'poi' or speaker_fingerprint is not None:
        return None

    text = text.lower()
    emotions = []
    for emotion in EMOTIONS:
        if emotion_is_contained(text, emotion):
            emotions.append(emotion['type'])
    if len(emotions) == 0:
        return None
    
    return ','.join(emotions)


def emotion_is_contained(text, emotion):
    return reduce(
        (lambda p,q: p or q),
        map(
            (lambda term: term in text),
            emotion['terms']
        )
    )
