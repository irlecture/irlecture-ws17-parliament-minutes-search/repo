# coding: utf-8
from datetime import datetime

MONTH_NUMBERS = {
    "Januar": 1,
    "Februar": 2,
    "März": 3,
    "M�rz": 3,
    "April": 4,
    "Mai": 5,
    "Juni": 6,
    "Juli": 7,
    "August": 8,
    "September": 9,
    "Oktober": 10,
    "November": 11,
    "Dezember": 12,
    "Mittwoch": 9
}

def parse(datestring):
    for k,v in MONTH_NUMBERS.items():
        datestring = datestring.replace(k,str(v))
    return datetime.strptime(datestring,"%d. %m %Y")
