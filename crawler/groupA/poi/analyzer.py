from groupA.poi.database import Database
from nltk import word_tokenize
from nltk import FreqDist

EMOTIONS = [
            {'type': 'Heiterkeit', 'terms': 'heiterkeit', 'frequency': 5412},
            {'type': 'Lachen', 'terms': 'lachen', 'frequency': 3360},
            {'type': 'Beifall', 'terms': 'beifall', 'frequency': 179827},
            {'type': 'Zustimmung', 'terms': 'zustimmung', 'frequency': 85},
            {'type': 'Widerspruch', 'terms': ['widerspruch', 'intervention'], 'frequency': 2156},
            {'type': 'Unruhe', 'terms': 'unruhe', 'frequency': 226},

            {'type': 'Frage', 'terms': ['zwischenfrage', 'nachfrage'], 'frequency': 399},
            {'type': 'Zuruf', 'terms': 'zuruf', 'frequency': 5221},
            {'type': 'Gegenruf', 'terms': 'gegenruf', 'frequency': 38},

            {'type': 'Gratulation', 'terms': ['glückwünsch', 'gratul'], 'frequency': 23}
            ]


def print_most_frequent_tokens(input, all_names):
    all_tokens = []
    for row in input:
        sentence = row[0]
        for name in all_names:
            sentence = sentence.replace(name, '')
        tokens = word_tokenize(sentence, 'german')
        all_tokens.extend(tokens)

    token_frequency = FreqDist(all_tokens)
    for token, frequency in token_frequency.most_common(350):
        print('{:20}: {:>5}'.format(token, frequency))


def print_token_overview(input, db):
    all_names = db.select_all_politician_names()
    all_names = [row[0] for row in all_names]
    print_most_frequent_tokens(input, all_names)


def start():
    db_file = '../../../data/db/db.sqlite'
    db = Database(db_file)
    pois = db.select_poi_where_speaker_is_null()
    # print_token_overview(pois, db)


start()

