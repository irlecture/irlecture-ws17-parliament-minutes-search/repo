#!/usr/bin/python
# coding: utf-8
import sys;
sys.path.append('./module');
import os;
import groupA;
import groupA.crawler;
from groupA.help_table_insertion import insert_help_tables

#1. settings
TXT_DIR = '../data/crawler';
DATABASE_URI = '../data/db/db.sqlite'
PROTOCOL_HTML_DIR = '../data/htmlProtocol';

#2. Set up Database
groupA.createDB(DATABASE_URI)


#3. download
if not "--nodl" in sys.argv:
    print("Fetching URLs")
    urls = groupA.crawler.get_urls()
    print("Downloading Protocolls")
    groupA.crawler.download(urls,TXT_DIR)


#3. scrape content
if not "--noparse" in sys.argv:
    for file in os.listdir(TXT_DIR):
       if file.endswith(".txt"):
           if sys.argv[-1].startswith("--") or sys.argv[-1] == file or len(sys.argv) < 2:
                print("Parsing: "+file)
                groupA.scape(os.path.join(TXT_DIR,file), DATABASE_URI, PROTOCOL_HTML_DIR)


# 4. insert additional data: emotions, speakers
if "--nohelptable" not in sys.argv:
    print('Inserting help tables: speakers, emotions, ...')
    insert_help_tables(DATABASE_URI)

if "--noparse" in sys.argv:
    sys.exit(0)
