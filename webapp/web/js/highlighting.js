function highlightResult(text, snippet, resultId, snippetIndex) {
    var borders = bordersOfHighlighting(text, snippet);
    var startHighlighting = borders[0];
    var endHighlighting = borders[1];
    var textBeforeHighlighting = text.slice(0, startHighlighting);
    var highlightedText = text.slice(startHighlighting, endHighlighting);
    var textAfterHighlighting = text.slice(endHighlighting, text.length);
    return "<span>"+ textBeforeHighlighting + "</span>" +
           "<span id=\"highlight-snippet-" + snippetIndex + "-" + resultId + "\" class=\"highlight-snippet\">" + highlightedText + "</span>" +
           "<span>" + textAfterHighlighting + "</span>";
}

function bordersOfHighlighting(text, snippet) {
    var startSnippet = text.indexOf(snippet);
    var deltaStart = startOfTextWithoutAnyAnchor(snippet);
    var deltaEnd = snippet.length - endOfTextWithoutAnyAnchor(snippet);
    var startHighlighting = startSnippet + deltaStart;
    var lengthHighlighting = snippet.length - deltaStart - deltaEnd;
    var endHighlighting = startHighlighting + lengthHighlighting;
    return [startHighlighting, endHighlighting];
}