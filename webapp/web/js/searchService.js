/**
	Handling of search actions.
*/
var MAX_TEXT_LENGTH = 500, 
searchResults,
ACTIVE_PAGE = 1,
MAX_PAGE, 
searchParams;


var lastQuerystring = "";
var modalText = "";

var url = "webservice/search";

var onSearch = function(){
ACTIVE_PAGE = 1;
searchParams = readSearchParams();
lastQuerystring = searchParams["value"] || " ";
//console.log('search parameters: ' + JSON.stringify(searchParams));
getSearchResult(searchParams);
};

document.addEventListener("keypress", function(key){
var pressedKey = key.which || key.keyCode;
if(pressedKey === 13) onSearch(); });
/**
*Get search parameter from searchmask
* @return searchParams list of the set search parameter
*/
function readSearchParams() {
	searchParams = {};
	addSearchParam(searchParams,"value", $("#search-query").val().trim());
	addSearchParam(searchParams,"topic", $("#search-topic").val().trim());
	if ($("#search-emotion").val()) addSearchParam(searchParams, "emotion", $("#search-emotion").val().join());
	if ($("#search-emotion-party").val()) addSearchParam(searchParams, "emotion_party", $("#search-emotion-party").val().join());
        
        if($("#search-politician-fp").val().length > 0 && $("#search-politician-fp").data("politician-name") == $("#search-politician").val().trim()){
            addSearchParam(searchParams,"politician-fp", $("#search-politician-fp").val().trim());
        } else {
            addSearchParam(searchParams,"politician", $("#search-politician").val().trim());
        }
        
	
	//console.log($("#search-party").val());
	if($("#search-party").val()) addSearchParam(searchParams,"party", $("#search-party").val().join(" "));
	addSearchParam(searchParams,"wp", $("#election-period").val());
	if($("#year-from").datepicker("getDate") || $("#year-to").datepicker("getDate")){
		var timeF = 0;
		var timeT = Math.floor(new Date() / 1000);
		if($("#year-from").datepicker("getDate")) timeF = $("#year-from").datepicker("getDate").getTime() / 1000 ;
		if($("#year-to").datepicker("getDate")) timeT = $("#year-to").datepicker("getDate").getTime() / 1000 + 86400;
		addSearchParam(searchParams,"timestamp",timeF + "-" + timeT);
	}
	addSearchParam(searchParams,"sort", $("#sort").val().trim());
	addSearchParam(searchParams,"limit", "10");
	addSearchParam(searchParams,"offset", "0");
	return searchParams;
}

function addSearchParam(searchParams,key, value) {
//console.log(key + " "+ value);
if (value.length > 0 && value != "0" || key === "offset") searchParams[key] = value;
}
/**Ajax Call for search Results
* @param searchParams  set parameter in searchmask
*/
function getSearchResult(searchParams) {
// console.log(searchParams);
$.ajax({
	url: url,
	crossOrigin: true,
	method: 'GET',
	data: searchParams,
	accept: 'application/json',
	success: function(response){
		if(!response.success){ 
			clearContainer();
			$("#amountResults").append("Anzahl Ergebnisse: 0");	
			return; //delete as soon as backend can send 400er responses.
		}
		searchResults = response.data;
		searchResults.count = response.count;
		window.localStorage.setItem("results", JSON.stringify(searchResults));
		insertResults(searchResults);
		suggestQuery(response.alternativeQuery);
                buildTaxo(response.taxo)
		if(ACTIVE_PAGE === 1) createPagination();
	},
	error: function onError(request, status, error) {
		console.log(request.responseText);
		
	}
});
}
//empty container
function clearContainer(){
	$("#results").empty();
	$("#pages").empty();
	$("#amountResults").empty();
	buildTaxo();
}

function buildTaxo(taxo){
    var $container = $("#taxo");
    $container.empty();
    $container.removeClass("taxoContainer");
    if(!taxo){return;}
    
    var hasItems = false;
    
    if(taxo.party && taxo.party.length > 1){
        hasItems = true;
        
        $container.append("<strong>Top Parteien zum Thema: </stong>");
        
        $(taxo.party).each(function(i, party){
            if(i > 0){
                $container.append(", ");
            }
            
            var $link = $("<a href='#'></a>");
            $link.html( party.name + " (" + party.count + ")");
            
            $link.click(function(){
                $("#search-politician").val("");
                $("#search-politician-fp").val("");
                $("#search-politician-fp").data("politician-name", "");               
                $("#search-party").selectpicker('deselectAll');
                $("#search-party").selectpicker('val',party.key);
                
                if(!Visible.visible){
                        toggleAdvancedSearch();
                }
                onSearch();
            });
            
            $container.append($link);
        });
        
        $container.append("</br>");
    }
    
    if(taxo.speaker && taxo.speaker.length > 1){
        hasItems = true;
        
        $container.append("<strong>Top Politiker zum Thema: </stong>");
        
        $(taxo.speaker).each(function(i, speaker){
            if(i > 0){
                $container.append(", ");
            }
            
            var $link = $("<a href='#'></a>");
            var name = speaker.name;
            if(speaker.party_name && speaker.party_name.length > 0){
                name += " - <i>" + speaker.party_name + "</i>";
            }
            name += " (" + speaker.count + ")";
            
            $link.html(name);
            
            $link.click(function(){
                $("#search-politician").val(speaker.name);
                $("#search-politician-fp").val(speaker.key);
                $("#search-politician-fp").data("politician-name", speaker.name);
                $("#search-party").selectpicker('deselectAll');
                
                if(!Visible.visible){
                        toggleAdvancedSearch();
                }
                onSearch();
            });
            
            $container.append($link);
        });
    }
    
    if(hasItems){
        $container.addClass("taxoContainer");
    }
}

/** Displays a suggestion for an alternative query (if the server sent some)
*  
* @param alternativeQuery an alternative query as sent from the backend or none
*/
function suggestQuery(q) {
var container = $("#querysuggest");
if (!q || !q.text && !q.emotion || q.text && q.text.trim() === $("#search-query").val().trim()){
	container.empty();
	return;
}
var text = "<a id=\"suggestedQuery\"><b>Suchen Sie: </b>";
if (q.text && q.text.trim() != ""){
	text += "<b>Inhalt:</b> " + q.text +", ";
}
if (q.politicianName){
	text += "<b>Redner:</b> " + q.politicianName +", ";
}
if (q.party){
	text += "<b>Fraktion:</b> " + q.party+", ";
}
if (q.emotion) {
	text += "<b>Emotion:</b> " + q.emotion + ", ";
}
text = text.substring(0,text.length-2);
text += "</a>";
container.html(text);
$("#suggestedQuery").click(function(){
	$("#search-query").val(q.text);
	$("#search-politician").val(q.politicianName);
	$("#search-politician-fp").val(q.politicianFp);
	$("#search-politician-fp").data("politician-name", q.politicianName);
	$("#search-party").selectpicker('deselectAll');
	$("#search-party").selectpicker('val',q.party);
	if (q.emotion) {
        $("#search-emotion").selectpicker('deselectAll');
        $("#search-emotion").selectpicker('val', q.emotion.split(','));
	}

	container.empty();
	if(!Visible.visible){
		toggleAdvancedSearch();
	}
	onSearch();
})
}

/**
*Creates the html structure for every response and inserts the result. 
*@param results the results for current query send from the backend
*/
function insertResults(results){
	var resultContainer = $("#results");
	var resultAmount = $("#amountResults");
	var numberResultsText = (results.count >= 10000) ? "mehr als 10.000 Ergebnisse" : "Anzahl Ergebnisse: " +  results.count;
	resultContainer.empty();
	resultAmount.empty();
	resultAmount.append(numberResultsText);
	$("#divCount").css("visibility", "visible");
	for(var i=0; i< 10; i++){
		if(results.length - 1 < i) break;
		var result = results[i];
		resultContainer.append(createSingleResult(result,i));
	}
	resultContainer.append("<div class=\"modal fade modal-speech\" id=\"modal-speech\" role=\"dialog\"></div>");
}
/**
* creating html structure for each relevant speech
* @param result, idx id of result
*/function createSingleResult(result, idx){
	var pdfUrl = buildPdfUrl(result);

	var text;
	modalText = addSlotsToText(result.text, result);

	if(result.snippets && result.snippets.length > 0){
		text="";
		for(var snippetIndex=0; snippetIndex<result.snippets.length;snippetIndex++){
		text += "<a class=\"document-link modalToggle\" onclick=\"createSpeechModal(" + idx + "," + snippetIndex + ")\">"+ result.snippets[snippetIndex] + "..." +"</a>";
		}
		text = textWithoutAnyAnchor(text)
	} else {
		// no snippets available
		text = truncText(textWithoutAnyAnchor(result.text));
		text = "<a class=\"document-link modalToggle\" onclick=\"createSpeechModal("+ idx + "," + 0 +")\">" + text + "</a>";
	}


	var html = [
		"<div class=\"row speech-result\" id=\"result-" + idx + "\">",
			"<div class=\"col-md-7\">",
				"<div class=\"speaker\">",
					"<div class =\"person\">" + result.speaker + "</div>",

					((result.party !== "") ? "<div class =\"party\">" + "<img src=\"media/parteien/" + result.party + ".png\">" + "</div>" : ""),
				"</div>",
				"<div class='snippets-in-result-list'>"+ text +"</div>",
			"</div>",
			"<div class=\"col-md-5 speech-metadata\">",
				"<div class='panel panel-default'>",
        			"<div class='panel-body'>",
						"<dl class='speech-metadata-list'>",
						"<dt>Datum:</dt> <dd>" + moment(result.timestamp, 'X').format('DD.MM.YYYY') + "</dd>",
						"<dt>Sitzung:</dt> <dd>" + result.s + ". Sitzung, " + result.wp + ". Wahlperiode <a target='_blank' href='"+pdfUrl+"'><i class='fa fa-file-pdf-o' aria-hidden='true'></i></a></dd>",
						"<dt>Tagesordnungspunkt:</dt> <dd class =\"top\">" + truncTextTop(result.bTop,idx) + "</dd>",
						"<dt>Emotionen im Parlament:</dt> <dd class='emotion-overview'>" + enrichEmotionsByEmoticonsAndCount(result.emotions) + "</dd>",
					"</div>",
				"</div>",
			"</div>",
		"</div>",
		"<div class=\"row\"><div class=\"col-md-12 result-annotation\">"+ generateResultAnnotation(result)+"</div></div>",
        "<div class=\"row\"><div class=\"col-md-12 result-seperator\"></div></div>"

	].join("");
	return html;
}


function createSpeechModal(resultId, snippetIndex){
	var snippet;
	var result = searchResults[resultId];

	if(result.snippets.length > 0){
		snippet = result.snippets[snippetIndex];
	}

	var modalText;
	if (result.snippets.length > 0) {
		modalText = addSlotsToText(highlightResult(result.text, snippet, resultId, snippetIndex), result);
    } else {
		modalText = addSlotsToText(result.text, result);
    }

	var html= [
		"<div class=\"modal-dialog\">",
		      "<!-- Modal content-->",
		      "<div class=\"modal-content\">",
				"<div class=\"modal-header\">",
			  	  	"<div class=\"speaker speaker-modal\">",
						"<div class =\"person\">" + result.speaker + "</div>",
						((result.party !== "") ? "<div class =\"party\">" + "<img src=\"media/parteien/" + result.party + ".png\">" + "</div>" : ""),
					"</div>",
			  	  "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>",
				"</div>",
				"<div class=\"modal-body\" id=\"modal-body\">",	
			  		"<div class=\"modal-speech\" id =\"modal-speech\">"+ modalText +"</div>",
				"</div>",
				"<div class=\"modal-footer\" id=\"modal-footer\">",
					"<a class=\"document-link\" onclick=\"openInNewTab(" + resultId + ")\">Mehr Kontext anzeigen</a>",
				"</div>",
		      "</div>",	
		"</div>"].join("");
		
	$("#modal-speech").empty().off();
	$("#modal-speech").append(html);
	$("#modal-speech").modal("show");
	if (result.snippets.length > 0)
		$("#modal-speech").on("shown.bs.modal", function() {
			$("#modal-body").animate({ scrollTop: $("#highlight-snippet-" + snippetIndex + "-" + resultId).position().top});
		});
}
/**Open whole protocol in new tab
* @param id 	id of the element in the result
*/
function openInNewTab(id) {
  var result = searchResults[id];

  var win = window.open("resultpage.html?id=" + result.id+"&wp="+ result.wp +"&s="+ result.s, '_blank');
  win.focus();
}
/**Show missing words in speech result
* @param result
*/
function generateResultAnnotation(result){
	var missingWords = [];
	if(lastQuerystring.charAt(0)=== "\"" && lastQuerystring.charAt(lastQuerystring.length - 1) === "\"") lastQuerystring = lastQuerystring.substring(1, lastQuerystring.length -2);
	var queryWords = lastQuerystring.split(" ");
	for (var i = 0; i < queryWords.length;i++){
		if(result.text.toLowerCase().indexOf(queryWords[i].toLowerCase()) == -1){
			missingWords[missingWords.length] = queryWords[i];
		} 
	}
	if (missingWords.length != 0){
		return "Fehlende Suchworte: " + missingWords.join(", ");
	}
	return ""
}

function onClickPagination(newpage){
if(newpage > MAX_PAGE) return;

if(newpage < 1) return;

ACTIVE_PAGE = newpage;
addSearchParam(searchParams,"offset", 10 * (ACTIVE_PAGE-1));
getSearchResult(searchParams);
onPagination(newpage);
}
/**
Pagination for results. Limit 10 per page
*/
function createPagination(){
var resultPagination = $("#pagination");
MAX_PAGE = Math.ceil(searchResults.count / 10.00);
var paginationHtml =
	["<nav aria-label=\"page-navigation\">",
		"<ul class=\"pagination\" id=\"pages\">",
		"</ul>", 
	"</nav>"];
resultPagination.append(paginationHtml.join(""));
onPagination(1);
}
/**
Pagination. Shows pages from -2 to +2 of current page and first as well as last page
@param newpage new current page
*/
function  onPagination(newpage) {
var paginationHtml= [],
startHtml = ["<li class=\"page-item\">",
"<a class=\"page-link\" onclick=\"onClickPagination(" + (newpage - 1) + ")\" href=\"#search-form\" aria-label=\"Previous\">",
				"<span aria-hidden=\"true\">&laquo;</span>",
				"<span class=\"sr-only\">Previous</span>" ,
			"</a>",
		"</li>"],
endHtml = ["<li class=\"page-item\">",
			"<a class=\"page-link\" onclick=\"onClickPagination(" + (newpage + 1) + ")\" href=\"#search-form\" aria-label=\"Next\">",
				"<span aria-hidden=\"true\">&raquo;</span>",
				"<span class=\"sr-only\">Next</span>",
			"</a>",
		"</li>"],
dots = "<li class=\"page-item custom-disabled\"><a class=\"page-link\" href=\"#search-form\">&hellip;</a></li>";

for(var i = Math.max(1, newpage - 2); i <= Math.min(newpage + 2, MAX_PAGE); i++){
	paginationHtml.push("<li class=\"page-item" + ((newpage === i) ? " active" : "") + "\"><a class=\"page-link\" onclick=\"onClickPagination(" + i + ")\" href=\"#search-form\">"+i+"</a></li>");
}
if(newpage > 3) { 
	if(newpage > 4) paginationHtml.unshift(dots); 
	paginationHtml.unshift("<li class=\"page-item\"><a class=\"page-link\" onclick=\"onClickPagination(" + 1 + ")\" href=\"#search-form\">"+1+"</a></li>"); 
}
if(newpage < MAX_PAGE - 2) { 
	if(newpage < MAX_PAGE - 3) paginationHtml.push(dots); 
	paginationHtml.push("<li class=\"page-item\"><a class=\"page-link\" onclick=\"onClickPagination(" + MAX_PAGE + ")\" href=\"#search-form\">"+MAX_PAGE+"</a></li>");
}

finalPaginationHtml = [].concat(startHtml, paginationHtml, endHtml);
$("#pages").empty();
$("#pages").append(finalPaginationHtml.join(""));
}

/**
@param result
@return link to protocol
*/
function buildPdfUrl(result) {
var wahlperiode = result.wp;
var sitzung = result.s.toString();
sitzung = "0".repeat(3 - sitzung.length) + sitzung;
return "http://dip21.bundestag.de/dip21/btp/" + wahlperiode + "/" + wahlperiode + sitzung + ".pdf";
}

/**
	Default pop up modal 
*/
function showModalOnDefault(){

}

/**
Truncates the text length of the speech result.
	If the text is longer than MAX_TEXT_LENGTH, it is shortend to the max length
	else keep full text
@param the speech text of the result i, result number i
@return speech
*/
function truncText(speech){
	if(speech.length > MAX_TEXT_LENGTH){
		return speech.substring(0, MAX_TEXT_LENGTH)+"&hellip;";
	} else{
		return speech;
	}
}

//Truncate text of Tagesordnungspunkt
function truncTextTop(speech, i){
var max_top_length = 110;
if(speech.length > max_top_length){
	return speech.substring(0,max_top_length)+" <span class=\"link-class\" onclick=\"expandTextTop("+ i +")\"><i class=\"fa fa-chevron-down\" aria-hidden=\"true\"></i>\n</span>";
} else{
	return speech;
}
}
var expandTextTop = function(i){
$("#result-" + i + " .top").empty();
$("#result-" + i + " .top").append(searchResults[i].bTop + " <span class=\"link-class\" onclick=\"cutTextTop("+ i +")\"><i class=\"fa fa-chevron-up\" aria-hidden=\"true\"></i>\n</span>");
};

var cutTextTop = function(i){
$("#result-" + i + " .top").empty();
$("#result-" + i + " .top").append(truncTextTop(searchResults[i].bTop, i));
};

$("#sort").change(function(){
    onSearch();      
});
