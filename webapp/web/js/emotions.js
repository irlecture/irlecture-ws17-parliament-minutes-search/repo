function addEmotionsToText(text, emotions, emotionParties, emotionTexts) {
    if (emotions.length < 1)
        return text;

    var parts = splitTextByEmotions(text);
    return insertEmotions(parts, emotions, emotionParties, emotionTexts);
}

function splitTextByEmotions(text) {
    return text.split("###E###");
}

function insertEmotions(parts, emotions, emotionParties, emotionTexts) {
    var text = "";
    var i;
    for (i = 0; i < emotions.length; i++) {
        var previousText = parts[i];
        var emotion = emotions[i];
        var party = emotionParties[i];
        var emotionText = emotionTexts[i];
        text += "<p>" + previousText + "</p>";
        text += buildEmotion(emotion, party, emotionText);
    }
    return text += "<p>" + parts[parts.length - 1] + "</p>";
}

function buildEmotion(emotion, party, emotionText) {
    var tag = "<p class=\"emotion-paragraph " + emotion + " " + party + "\">";
    tag += "<span class='emoticon'>"+chooseEmoticon(emotion)+"</span>";
    tag += emotionText;
    tag += "</p>";
    return tag;
}

function chooseEmoticon(emotion) {
    if (emotion.includes(","))
        return chooseMultiEmoticons(emotion.split(","));
    else
        return chooseSingleEmoticon(emotion);

}

function chooseMultiEmoticons(emotions) {
    var emoticons = [];
    var i;
    for (i = 0; i < emotions.length; i++)
        emoticons.push(chooseSingleEmoticon(emotions[i]))
    return emoticons.join(' ');
}

function chooseSingleEmoticon(emotion) {
    emotion = emotion.trim();
    switch (emotion) {
        case "Beifall":
            return "&#128079;";
        case "Frage":
            return "&#10068;";
        case "Gegenruf":
            return "&#128495;";
        case "Gratulation":
            return "&#127873;";
        case "Heiterkeit":
            return "&#9786;";
        case "Lachen":
            return "&#128515;";
        case "Unruhe":
            return "&#128491;";
        case "Widerspruch":
            return "&#128544;";
        case "Zuruf":
            return "&#128495;";
        case "Zustimmung":
            return "&#128077;";
    }
}

function enrichEmotionsByEmoticonsAndCount(emotions) {
    var emotionHash = list2hash(emotions);
    var i;
    var result = "";
    Object.keys(emotionHash).forEach(function (emotion) {
        var count = emotionHash[emotion];
        result += "<span class='emotion-list-item " + emotion + "'>";
        result += "<span class='emoticon'>" + chooseEmoticon(emotion) + "</span>";
        result += emotion + " (" + count + ")</span>";
    });
    return result;
}

function list2hash(list) {
    var h = {};
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i] in h)
            h[list[i]] += 1;
        else
            h[list[i]] = 1;
    }
    return h;
}

function startOfTextWithoutEmotionAnchor(text) {
    if (text.startsWith('#'))
        return 1 + startOfTextWithoutEmotionAnchor(text.slice(1));
    else if (text.startsWith('E##'))
        return 3 + startOfTextWithoutEmotionAnchor(text.slice(3));
    else
        return 0;
}

function endOfTextWithoutEmotionAnchor(text) {
    if (text.endsWith('#'))
        return endOfTextWithoutEmotionAnchor(text.slice(0, text.length - 1));
    else if (text.endsWith('##E'))
        return endOfTextWithoutEmotionAnchor(text.slice(0, text.length - 3));
    else
        return text.length;
}
