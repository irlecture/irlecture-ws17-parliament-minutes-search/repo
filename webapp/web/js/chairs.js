function addChairsToText(text, chairTexts, chairSpeakers) {
    if (chairTexts.length < 1)
        return text;

    var parts = splitTextByChairs(text);
    return insertChairs(parts, chairTexts, chairSpeakers);
}

function splitTextByChairs(text) {
    return text.split("###C###");
}

function insertChairs(parts, chairTexts, chairSpeakers) {
    var text = "";
    var i;
    for (i = 0; i < chairTexts.length; i++) {
        var previousText = parts[i];
        var chairText = chairTexts[i];
        var chairSpeaker = chairSpeakers[i];
        text += "<p>" + previousText + "</p>";
        text += buildChair(chairText, chairSpeaker);
    }
    return text += "<p>" + parts[parts.length - 1] + "</p>";
}

function buildChair(chairText, chairSpeaker) {
    var tag = "<p class='chair-paragraph'>";
    tag += chairSpeaker + ': ';
    tag += "<i>" + chairText + "</i>";
    tag += "</p>";
    return tag;
}
