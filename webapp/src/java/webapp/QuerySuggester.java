package webapp;

import java.sql.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;

public class QuerySuggester{

    private static int MULTI_NAME_MULTIPLICATOR = 100;

    Connection conn;
    static private HashMap<String,String> partyMapper;
    static{
        partyMapper = new HashMap<String,String>();
        partyMapper.put("cdu", "cducsu");
        partyMapper.put("csu","cducsu");
        partyMapper.put("spd", "spd");
        partyMapper.put("linke", "linke");
        partyMapper.put("fdp", "fdp");
        partyMapper.put("gruene", "gruene");
        partyMapper.put("grüne", "gruene");
        partyMapper.put("afd", "afd");
    }

    static private Set<String> EMOTIONS = new HashSet<String>(Arrays.asList(
        "heiterkeit", "lachen", "beifall", "zustimmung", "widerspruch", "unruhe", "frage", "zuruf", "gegenruf", "gratulation"
    ));

    public static class Query{
        public String text;
        public String politicianName;
        public String politicianFp;
        public String party;
        public String emotion;

        Query(String text, String politician, String party,String emotion){
            this.text = text;
            this.politicianName = politician;
            this.party = party;
            this.emotion = emotion;
        }

        public Query clone(){
            return new Query(text,politicianName,party,emotion);
        }
        
    }

    QuerySuggester(String sqlitedb) throws ClassNotFoundException{
        try{
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:" + sqlitedb);
        } catch (SQLException e){
            System.out.println(sqlitedb);
            e.printStackTrace();
        }
    }

    public Query suggest(Query original) throws SQLException{
        if (original.text == null)
            return original;
        return suggestEmotion(suggestParty(suggestPolitician(original)));
    }

    public Query suggestEmotion(Query original) {
        Set<String> suggestedEmotions = new HashSet<String>();
        Query suggested = original.clone();
        for (String word : original.text.split(" ")) {
            String preprocessedWord = word.toLowerCase();
            if (EMOTIONS.contains(preprocessedWord)) {
                suggestedEmotions.add(preprocessedWord);
                suggested.text = suggested.text.replace(word, "");
            }
        }
        if (suggestedEmotions.size() == 0)
            return original;

        suggested.emotion = "";
        for (String e : suggestedEmotions)
            suggested.emotion += e.substring(0,1).toUpperCase() + e.substring(1).toLowerCase() + ",";
        suggested.emotion = suggested.emotion.substring(0, suggested.emotion.length() - 1);
        return suggested;
    }
    
    
    public Query suggestParty(Query original){
        // Do not suggest a party if a politician is provided
        if (original.politicianName != null || original.party != null){
            return original;
        }
        for (String word : original.text.split(" ")){
            String lword = word.toLowerCase();
            String suggestion = partyMapper.get(lword);
            
            if (suggestion != null){
                Query suggested = original.clone();
                suggested.text = suggested.text.replace(word,"");
                suggested.party = suggestion;
                return suggested;
            }
        }
        return original;
    }
    
    private static class PolRepl{
        String fp;
        LinkedList<String> words;
        String name;
        int importance;
        
        PolRepl(String word, String fp, String name, int importance){
            this.words = new LinkedList<String>();
            this.words.add(word);
            this.fp = fp;
            this.name = name;
            this.importance = importance;
        }
    }

    public Query suggestPolitician(Query original) throws SQLException{
        if (original.politicianName != null || original.party != null){
            return original;
        }
        String[] words = original.text.split(" ");

        String sql = "SELECT fingerprint, name, activity_measure FROM speakers WHERE name LIKE ?";        
        PreparedStatement pstmt = conn.prepareStatement(sql);

        HashMap<String,PolRepl> possible = new HashMap<String,PolRepl>();

        for( int i = 0; i < words.length; i++ ){
            pstmt.setString(1, "%" + words[i] + "%");
            
            ResultSet rs = pstmt.executeQuery();
            while ( rs.next() ) {
                String fp = rs.getString("fingerprint");
                String name = rs.getString("name");
                int activity_measure = rs.getInt("activity_measure");
                
                String matchedword = findWordInName(name,words[i]);
                if (matchedword != null){
                    if (isNameStopword(matchedword)){
                        insertOrUpdate(possible,words[i],fp,name,0);
                    }else{
                        insertOrUpdate(possible,words[i],fp,name,activity_measure);
                    }
                }
            }
        }
        
        PolRepl best = null;
        for (PolRepl r : possible.values()){
            if(r.importance > 0 && (best == null || r.importance > best.importance)){
                best = r;
            }
        }
        
        if (best == null){
            return original;
        }
        
        Query suggestion = original.clone();
        for (String word : best.words){
            suggestion.text = suggestion.text.replace(word, "");
        }
        suggestion.text = suggestion.text.replaceAll("\\s+"," ").trim();
        suggestion.politicianFp = best.fp;
        suggestion.politicianName = best.name;
        
        return suggestion;
    }

    private void insertOrUpdate(HashMap<String,PolRepl> table, String word, String fp, String name, int importance){
        PolRepl in = table.get(fp);
        if (in == null){
            table.put(fp,new PolRepl(word,fp,name,importance));
        }else{
            in.importance+=importance;
            in.importance*=MULTI_NAME_MULTIPLICATOR;
            in.words.add(word);
        }
    }

    private boolean isNameStopword(String word){
        if (word.length() == 0){
            return true;
        }
        // Filter titles in names (Dr. Prof. etc...)
        if (word.endsWith(".")){
            return true;
        }
        // Filter words in names that dont't start with an uppercase letter (von, zu, der etc...)
        if (!Character.isUpperCase(word.charAt(0))){
           return true;
        }
        return false;
    }

    private String findWordInName(String fullname, String word){
        List<String> words = Arrays.asList(fullname.split(" |,"));
        word = word.toLowerCase();
        for (String w : words){
            if (w.toLowerCase().equals(word)){
                return w;
            }
        }
        return null;
    }

}
