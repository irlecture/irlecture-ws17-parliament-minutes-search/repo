FROM python:3.5
RUN pip install idna six lxml requests nose
RUN echo "deb http://http.debian.net/debian jessie-backports main" >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y -t jessie-backports openjdk-8-jdk && \
    apt-get install -y ant && \
    apt-get install -y make && \
    rm -rf /var/lib/apt/lists/*

ENV PYTHONPATH /src/crawler/module/
ENV j2ee_server_home /
ENV j2ee_platform_classpath /src/compiledeps/j2ee/tomcat-servlet-api-7.0.6.jar
ENV libs_CopyLibs_classpath /src/compiledeps/netbeans/org-netbeans-modules-java-j2seproject-copylibstask.jar

WORKDIR /src
ENTRYPOINT ["make"]

