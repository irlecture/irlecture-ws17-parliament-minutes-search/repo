/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package groupa;

import java.io.IOException;
import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.document.IntPoint;

import org.apache.lucene.search.Sort;

//import org.apache.lucene.document.SortedDocValuesField;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.SimpleFragmenter;
import org.apache.lucene.search.highlight.NullFragmenter;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyReader;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.facet.FacetsCollector;
import org.apache.lucene.facet.FacetResult;
import org.apache.lucene.facet.Facets;
import org.apache.lucene.facet.FacetsConfig;
import org.apache.lucene.facet.LabelAndValue;
import org.apache.lucene.facet.taxonomy.FastTaxonomyFacetCounts;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.SortField.Type;

/**
 * Klasse für Suche im Index
 */
public class searcher {
    
   index ind;
   String taxoDirectoryPath;
   Connection conn;
    
   /**
    * Konstruktor
    * @param indexDirectoryPath
    * @throws IOException 
    */
    public searcher(String indexDirectoryPath, String sqlFileFilePath) throws IOException, ClassNotFoundException, SQLException {
        ind = new index(indexDirectoryPath);
        taxoDirectoryPath = indexDirectoryPath.concat("/../taxoIndex");
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:" + sqlFileFilePath);
    }
    
    /**
     * Überladung: Einfache suche mit String
     * @param querystr
     * @return searchResult
     * @throws IOException
     * @throws ParseException 
     */
    public searchResult search(String querystr, String sortValue)  throws IOException, ParseException, InvalidTokenOffsetsException {
        return search(querystr, 0, 10, sortValue);
    }
    
    /**
     * Überladung: Einfache suche mit String und Paging
     * @param querystr
     * @param offset
     * @param limit
     * @return searchResult
     * @throws IOException
     * @throws ParseException 
     */
    public searchResult search(String querystr, Integer offset, Integer limit, String sortValue)  throws IOException, ParseException, InvalidTokenOffsetsException {
        Query q = new QueryParser("text", ind.analyzer).parse(querystr);
        return search(q , offset, limit, sortValue);
    }
    
    /**
     * Such Function für Boolean Query (alte Funktion)
     * @param values
     * @param offset
     * @param limit
     * @return searchResult
     * @throws IOException
     * @throws ParseException 
     */
    public searchResult boolSearch(HashMap<String, String> values, Integer offset, Integer limit)  throws IOException, ParseException, InvalidTokenOffsetsException {
        
        BooleanQuery.Builder BooleanQueryBuilder = new BooleanQuery.Builder();
        
        Integer minMatch = 0; 
        boolean date = false;
        String sortValue ="";
        for(Map.Entry<String, String> entry : values.entrySet()) {
            if(entry.getKey().equals("timestamp")){
                String[] v = entry.getValue().split("-");
                Query queryNumeric = IntPoint.newRangeQuery("_timestamp", Integer.parseInt(v[0]), Integer.parseInt(v[1]));
                BooleanQueryBuilder.add(queryNumeric,BooleanClause.Occur.SHOULD);
                minMatch += 1;
                date = true;
                continue;
            }
            if(entry.getKey().equals("wp")){
                date = true;
            }
            if(entry.getKey().equals("sort")){
                sortValue = entry.getValue();
                continue;
            }
            
            /*no tokenize speaker fingerprint*/
            if(entry.getKey().equals("speaker_fp")){
                 Query queryTerm = new TermQuery(new Term("speaker_fp", entry.getValue()));
                 BooleanQueryBuilder.add(queryTerm,BooleanClause.Occur.SHOULD);
                minMatch += 1;
                continue;
            }
            
            Query q = new QueryParser(entry.getKey(), ind.analyzer).parse(entry.getValue());
            BooleanQueryBuilder.add(q,BooleanClause.Occur.SHOULD);
            minMatch += 1;
        }

        BooleanQueryBuilder.setMinimumNumberShouldMatch(minMatch);
        BooleanQuery bq = BooleanQueryBuilder.build();

        return search(bq, offset, limit, sortValue);
    }   
    
    /**
     * Führt query auf Index aus und gibt ergebniss zurück
     * @param q
     * @param offset
     * @param limit
     * @return
     * @throws IOException
     * @throws ParseException
     * @throws InvalidTokenOffsetsException 
     */
    private searchResult search(Query q, Integer offset, Integer limit, String sortValue)  throws IOException, ParseException, InvalidTokenOffsetsException {
        searchResult res = new searchResult();
        
        int hits_max = 10000;
        IndexReader reader = DirectoryReader.open(ind.index);
        IndexSearcher searcher = new IndexSearcher(reader);
        TopDocs docs;
        Sort sorter = new Sort();
        String field = "timestamp_"; 
        Type type = Type.LONG; 
        //the different sorting options ascending, descending or relevance
        if(sortValue.equals("asc")){
            SortField sortField = new SortField(field, type, false);
            sorter.setSort(sortField);
            docs = searcher.search(q, hits_max, sorter);

        } else if(sortValue.equals("desc")){
            SortField sortField = new SortField(field, type, true);
            sorter.setSort(sortField);
            docs = searcher.search(q, hits_max, sorter);
        } else{
            docs = searcher.search(q, hits_max);
        }

       // TopDocs docs = searcher.search(q, hits_max);
        ScoreDoc[] hits = docs.scoreDocs;

        res.count = toIntExact(docs.totalHits);
        
        if (res.count > hits_max){
            res.count = hits_max;
        }
    
        int start = offset;
        int end = offset+limit;
        if(end > res.count)
            end = res.count;

        //Uses HTML &lt;B&gt;&lt;/B&gt; tag to highlight the searched terms
        Formatter formatter = new SimpleHTMLFormatter();
        String s = q.toString();
        s = s.substring(0,s.length()-2);
        String query = "";
        Query hq = q;
        int wspace =0;
        int queryStart = s.indexOf("text");   
        //exclude values of advanced search for highlighting        
         if(queryStart!= -1){ 
             s = s.replace(")","");
           // System.out.println(s);
            for(int i=queryStart+5; i < s.length();i++){
                 if(s.charAt(i)==' ') wspace = i;
                 if(s.charAt(i) == ':'){
                    query += s.substring(queryStart, wspace);
                    queryStart = s.indexOf("text",wspace -1);
                    if(queryStart == -1) break;
                    i = queryStart+5;
                    query += " ";
                 } 
                 if(i == s.length()-1) query += s.substring(queryStart, s.length());
            }
            query = "(" + query + ")";
           // System.out.println(query);
        } else{
            int tend = 0;
            int tstamp = s.indexOf("_timestamp");
            if(tstamp != -1) {
                tend = s.indexOf("]", tstamp);
                query = s.substring(0, tstamp) + s.substring(tend+1, s.length());
            } else query = s;
         }
            hq = new QueryParser("text", ind.analyzer).parse(query);
        //used to markup highlighted terms found in the best sections of a text
        Highlighter highlighter = new Highlighter(formatter, new QueryScorer(hq));
	      highlighter.setTextFragmenter(new SimpleFragmenter(60));

        Highlighter wordHighlighter = new Highlighter(formatter, new QueryScorer(hq));
        wordHighlighter.setTextFragmenter(new NullFragmenter());
       
    
        res.docs = new ArrayList<doc>();
        for(int i=start; i<end; i++)
	      {
            int docId = hits[i].doc;
            Document d = searcher.doc(docId);
            
            doc r = new doc();
            r.id = Integer.parseInt(d.get("id"));
            r.speaker = d.get("speaker");
            r.speaker_fp = d.get("speaker_fp");
            //r.text = d.get("text");
            r.party = d.get("party");
            r.wp = Integer.parseInt(d.get("wp"));
            r.s = Integer.parseInt(d.get("s"));
            r.bTop =  d.get("bTop");
            r.timestamp = Integer.parseInt(d.get("timestamp"));
            r.emotions = Arrays.asList(d.getValues("emotion"));
            r.emotionParties = Arrays.asList(d.getValues("emotion_party"));
            r.emotionTexts = Arrays.asList(d.getValues("emotion_text"));
            r.emotionPositions = Arrays.asList(d.getValues("emotion_position"));
            r.chairTexts = Arrays.asList(d.getValues("chair_text"));
            r.chairSpeaker = Arrays.asList(d.getValues("chair_speaker"));
            r._score = hits[i].score;
            
            //Create token stream
            String text = d.get("text");

            TokenStream tokens = ind.analyzer.tokenStream("text", new StringReader(text));
            String textHighlight = wordHighlighter.getBestFragment(tokens, text);
            r.text = textHighlight == null ? text : textHighlight;
            tokens.reset();
           
            tokens = ind.analyzer.tokenStream("text", new StringReader(text));
            String[] frags = highlighter.getBestFragments(tokens, text, 8);

           


            r.snippets = new ArrayList<>();
            for (String frag : frags)
            {
                r.snippets.add(frag);
            }
            
            res.docs.add(r);
        }
        
        /*Start Taxo*/
        FSDirectory taxoDirectory = FSDirectory.open((new java.io.File(taxoDirectoryPath)).toPath());
        TaxonomyReader taxoReader = new DirectoryTaxonomyReader(taxoDirectory);
        FacetsCollector fc = new FacetsCollector();
        FacetsCollector.search(searcher, q, 10, fc);
        FacetsConfig facetConfig = new FacetsConfig();
        
        Map<String, List<Map<String,String>>> taxoList = new HashMap<>();        
        Facets facets = new FastTaxonomyFacetCounts(taxoReader, facetConfig, fc);

        FacetResult fr = facets.getTopChildren(10, "f_party");
        
        if(fr != null){
            if (fr.labelValues.length > 0){
                List<Map<String,String>> facetValues = new ArrayList<>();

                for(int i=0; i< fr.labelValues.length; i++){
                    facetValues.add(getPartyFacet(fr.labelValues[i]));
                }

                taxoList.put("party",facetValues);
            }

            fr = facets.getTopChildren(10, "f_speaker");
            if (fr.labelValues.length > 0){
                List<Map<String,String>> facetValues = new ArrayList<>();

                for(int i=0; i< fr.labelValues.length; i++){
                    facetValues.add(getSpeakerFacet(fr.labelValues[i]));
                }

                taxoList.put("speaker", facetValues);
            }
        }
        taxoReader.close();
        res.taxo = taxoList;
        /*End Taxo*/
        
        reader.close();
        

        return res;
    }
    
    private Map<String, String> getSpeakerFacet(LabelAndValue lv) {
        Map<String, String> facetValue = new HashMap<>();
        facetValue.put("key",  lv.label);
        facetValue.put("count",  lv.value.toString());
        
        try {
            String sql = "SELECT name, party "
            + "FROM speakers "
            + "WHERE fingerprint LIKE ? "
            + "LIMIT 0, 1";

            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, lv.label);

            ResultSet speakers = pstmt.executeQuery();
            if(speakers.next()){
                facetValue.put("name",  speakers.getString("name"));
                facetValue.put("party",  speakers.getString("party"));
                facetValue.put("party_name",  getPartyName(speakers.getString("party")));
            }
            
        } catch (Exception e) {
            facetValue.put("name",  lv.label);
            facetValue.put("party",  "");
             facetValue.put("party_name",  "");
        }
        
        return facetValue;
    }
    
    private Map<String, String> getPartyFacet(LabelAndValue lv){
        Map<String, String> facetValue = new HashMap<>();
        facetValue.put("key",  lv.label);
        facetValue.put("count",  lv.value.toString());
        facetValue.put("name",  getPartyName(lv.label));
        
        return facetValue;
    }
    
    private String getPartyName(String PartySlug){
        switch (PartySlug) {
            case "cducsu": return "CDU/CSU";
            case "spd": return "SPD";
            case "linke": return "Die Linke";
            case "fdp": return "FDP";
            case "gruene": return "BÜNDNIS 90/DIE GRÜNEN";
            case "afd": return "AfD";
            default: return PartySlug;
        }
    }
    
}
