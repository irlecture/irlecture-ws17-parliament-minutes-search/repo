/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package groupa;

import java.util.ArrayList;
import java.util.List;

/**
 * Entity: Doc
 * Objekt, welches für den Index und die Rückgabe in der Suche verwendet wird
 */
public class doc {
    public Integer id;
    public String speaker = "";
    public String speaker_fp = "";
    public String text = "";
    public String party = "";
    public Integer wp = 0;
    public Integer s = 0;
    public String bTop = "";
    public String url = "";
    public Integer timestamp;
    public ArrayList<String> snippets = null;
    public List<String> emotions = new ArrayList<>();
    public List<String> emotionParties = new ArrayList<>();
    public List<String> emotionTexts = new ArrayList<>();
    public List<String> emotionPositions = new ArrayList<>();
    public List<String> chairTexts = new ArrayList<>();
    public List<String> chairSpeaker = new ArrayList<>();
    public float _score = 0;
}
