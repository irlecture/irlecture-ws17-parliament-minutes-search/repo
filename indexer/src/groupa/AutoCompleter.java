package groupa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.search.suggest.InputIterator;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.lucene.search.suggest.Lookup;

public class AutoCompleter{
    
    private AnalyzingSuggester suggester;
    private ReadWriteLock rwlock;
    
    private Directory tempDir;
    private String tempFilePrefix;
    private Analyzer analyzer;

    public AutoCompleter(Directory tempDir, String tempFilePrefix, Analyzer analyzer) {
        this.tempDir = tempDir;
        this.tempFilePrefix = tempFilePrefix;
        this.analyzer = analyzer;
        rwlock = new ReentrantReadWriteLock();
    }
    
    public AutoCompleter() throws IOException {
        this(FSDirectory.open(Paths.get("/tmp")), "lac-", new StandardAnalyzer());
    }
    
    public List<Lookup.LookupResult> suggest(String input, int number) throws IOException{
        if (suggester == null){
            //The suggester has not yet been created. Return an empty list.
            return new LinkedList<Lookup.LookupResult>();
        }
        List<Lookup.LookupResult> result;
        rwlock.readLock().lock();
        try{
            result = suggester.lookup(input, false, number);
        }finally{
            rwlock.readLock().unlock();
        }
        return result;
    }
    
    public void loadLogfile(String logfile)throws FileNotFoundException, IOException{
        loadLogfile(logfile,0);
    }
    
    public void loadLogfile(String logfile, int reloadIntervall)throws FileNotFoundException, IOException{
        //Create a new suggester and load the lofile into it
        try{
            LogfileIterator it = new LogfileIterator(logfile);
            AnalyzingSuggester newsuggester = new AnalyzingSuggester(tempDir, tempFilePrefix, analyzer);
            newsuggester.build(it);

            //swap the old Suggester with the new one. 
            //Do it synchronized with suggest() so we dont replace it while there is a suggestion-lookup running in another thread
            rwlock.writeLock().lock();
            try{
                suggester = newsuggester;
            }finally{
                rwlock.writeLock().unlock();
            }

        } catch(FileNotFoundException e){
            //This just means the logfile is not there yet. Ignore it.
        }
        
        //Periodically reload logfile to get latest queries
        //AnalyzingSuggester does not support updating, so we have to complete reload everything
        if (reloadIntervall > 0){
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                try{
                    loadLogfile(logfile,reloadIntervall);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }, reloadIntervall);
        }
    }
    
    private static class LogfileIterator implements InputIterator{
        
        BufferedReader r;
        HashMap<String,Integer> counts = new HashMap<String,Integer>();
        int currentWeight = 0;
        
        public LogfileIterator(String logfile) throws FileNotFoundException{
            File f = new File(logfile);
            FileReader fr = new FileReader(f);
            r = new BufferedReader(fr);
        }

        @Override
        public long weight() {
            return currentWeight;
        }

        @Override
        public BytesRef payload() {
            throw new UnsupportedOperationException("Not supported");
        }

        @Override
        public boolean hasPayloads() {
            return false;
        }

        @Override
        public Set<BytesRef> contexts() {
            throw new UnsupportedOperationException("Not supported");
        }

        @Override
        public boolean hasContexts() {
            return false;
        }

        @Override
        public BytesRef next() throws IOException {
            String line;
            line = r.readLine();
            if(line == null){
                r.close();
                return null;
            }
            String[] parts = line.split(", ");
            String word = parts[2];
            
            //Count how often we have seen a query. Use the count as weight. The Analyzer will chose the duplicate with the heighets weight (the correct one)
            Integer currentWeightObj = counts.get(word);
            if(currentWeightObj != null){
                currentWeight = currentWeightObj;
            }else{
                currentWeight = 0;
            }
            currentWeight++;
            counts.put(word, currentWeight);
            
            return new BytesRef(word.getBytes("UTF-8"));
        }
    
    }

}