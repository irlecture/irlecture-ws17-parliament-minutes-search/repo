/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package groupa;

import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.facet.FacetField;
import org.apache.lucene.facet.FacetsConfig;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyWriter;
import org.apache.lucene.store.FSDirectory;

//import org.apache.lucene.document.SortedDocValuesField;
//import org.apache.lucene.util.BytesRef;


import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Klasse: indexer
 * dient zur überführung der Daten in den Index
 */
public class indexer {
    
    index ind;
    String taxoDirectoryPath;

    private doc currentDocument;
    
    /**
     * Konstruktor
     * erwartet Pfad zum Order des Indexes
     * @param indexDirectoryPath
     * @throws IOException 
     */
    public indexer(String indexDirectoryPath) throws IOException {
        ind = new index(indexDirectoryPath);
        taxoDirectoryPath = indexDirectoryPath.concat("/../taxoIndex");
    }
    
    /**
     * Überführt Daten aus SqLight in den Index
     * @param sqlFileFilePath
     * @throws IOException
     * @throws ParseException
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public void indexSqLite(String sqlFileFilePath) throws IOException, ParseException, ClassNotFoundException, SQLException {
        ind.clearIndex();
        
        Class.forName("org.sqlite.JDBC");
        Connection c = DriverManager.getConnection("jdbc:sqlite:" + sqlFileFilePath);
        
        Statement stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery(queryForIndexerData());
        
        ArrayList<doc> docs = new ArrayList<>();
        SpeechBuilder speechBuilder = new SpeechBuilder();
        while ( rs.next() ) {
            speechBuilder.addItemAsResultSet(rs);
            currentDocument = speechBuilder.getNextSpeechAsDocument();
            if (currentDocument != null)
                docs.add(currentDocument);
        }
        docs.add(speechBuilder.getLastSpeechAsDocument());
        
        addDocs(docs);
        
        stmt.close();
    }

    public String queryForIndexerData() {
        return "SELECT speak.id, "
            + "     IFNULL(speak.speaker, '') AS speaker,"
            + "     IFNULL(speak.speaker_fp, '') AS speaker_fp,"
            + "     IFNULL(speak.text, '') as text,"
            + "     IFNULL(speak.speaker_party, '') as party,"
            + "     IFNULL(speak.emotion, '') as emotion,"
            + "     IFNULL(speak.type, '') as speech_type,"
            + "     speak.wp,"
            + "     speak.s,"
            + "     IFNULL(tops.topic, '') as bTop,"
            + "     IFNULL(protocol.url,'') as url,"
            + "     strftime('%s', IFNULL(protocol.date, 'now')) as timestamp"
            + " FROM speak"
            + " LEFT JOIN tops"
            + "     ON tops.id = speak.top_id"
            + " LEFT JOIN protocol"
            + "     ON protocol.wp = speak.wp"
            + "     AND protocol.s = speak.s"
            + " WHERE speak.type = 'speech'"
            + "    OR ( speak.type = 'poi' AND emotion IS NOT NULL )"
            + "    OR speak.type = 'chair'"
            + " ORDER BY speak.wp, speak.s, speak.sequence;";
    }

    /**
     * überfphrt liste von doc-Objekten in den Index
     * @param docs
     * @throws IOException 
     */
    private void addDocs(ArrayList<doc> docs) throws IOException {
        IndexWriterConfig  config = new IndexWriterConfig(ind.analyzer);
        IndexWriter indexWriter = new IndexWriter(ind.index, config);
        
        FacetsConfig facetConfig = new FacetsConfig();
        FSDirectory taxoDirectory = FSDirectory.open((new java.io.File(taxoDirectoryPath)).toPath());
        DirectoryTaxonomyWriter taxoWriter = new DirectoryTaxonomyWriter(taxoDirectory);
        
        for( doc d: docs )
        {
            Document doc = new Document();
            doc.add(new StringField("id", d.id.toString(), Field.Store.YES));
            doc.add(new TextField("text", d.text, Field.Store.YES));
            doc.add(new TextField("speaker", d.speaker, Field.Store.YES));
            doc.add(new StringField("speaker_fp", d.speaker_fp, Field.Store.YES));
            doc.add(new TextField("party", d.party, Field.Store.YES));
            doc.add(new TextField("bTop", d.bTop, Field.Store.YES));
            doc.add(new StringField("wp", d.wp.toString(), Field.Store.YES));
            doc.add(new StringField("s",  d.s.toString(), Field.Store.YES));
            doc.add(new NumericDocValuesField("timestamp_", d.timestamp));
            doc.add(new StoredField("timestamp_", d.timestamp));
            doc.add(new StringField("timestamp",  d.timestamp.toString(), Field.Store.YES));
            doc.add(new StringField("url",  d.url, Field.Store.YES));
            doc.add(new IntPoint("_timestamp",  d.timestamp));
            
            if (!d.speaker_fp.isEmpty()){
                doc.add(new FacetField("f_speaker", d.speaker_fp));
            }
            if (!d.party.isEmpty()){
                doc.add(new FacetField("f_party", d.party));
            }

            doc = addEmotions(doc, d);
            doc = addChairs(doc, d);
            indexWriter.addDocument(facetConfig.build(taxoWriter, doc));
        }
        
        indexWriter.close();
        taxoWriter.close();
    }
    
    private Document addEmotions(Document luceneDoc, doc temporaryDoc) {
        if (temporaryDoc.emotions.size() < 1)
            return luceneDoc;

        for (String e : temporaryDoc.emotions)
            luceneDoc.add(new TextField("emotion", e, Field.Store.YES));

        for (String p : temporaryDoc.emotionParties)
            luceneDoc.add(new TextField("emotion_party", p, Field.Store.YES));

        for (String t : temporaryDoc.emotionTexts)
            luceneDoc.add(new StoredField("emotion_text", t));

        return luceneDoc;
    }

    private Document addChairs(Document luceneDoc, doc temporaryDoc) {
        if (temporaryDoc.chairTexts.size() < 1)
            return luceneDoc;

        for (String t : temporaryDoc.chairTexts)
            luceneDoc.add(new StoredField("chair_text", t));

        for (String p : temporaryDoc.chairSpeaker)
            luceneDoc.add(new StoredField("chair_speaker", p));

        return luceneDoc;
    }

}
