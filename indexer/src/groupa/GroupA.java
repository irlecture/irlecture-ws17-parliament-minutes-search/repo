/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package groupa;

import java.io.File;

/**
 *
 * @author falk
 */
public class GroupA {

    /**
     * consolen Funktion
     * Argument 1 ist der name der auszuführenden Funktion,
     * wird allerdings derzeit nicht ausgewertet
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Indexer Start");
       
        if (args.length < 2){
             System.out.println("no arguments set. Example: indexing <Path to SqLite DB> <Path to index Folder>");
             System.exit(0);
        }
        
        String sqlLiteFilePath = args[1];
        File f = new File(sqlLiteFilePath);
        if (!f.exists()){
            System.out.println("SqLite file '" + sqlLiteFilePath + "' not exists");
            System.exit(0);
        }
        
        try {
            if(args[0].equals("indexing") || args[0].equals("build")){
                if (args.length < 3){
                    System.out.println("Argument 3 failed. Please set a Path to index-folder");
                    System.exit(0);
                }
                
                String indexDirectoryPath = args[2];
                f = new File(indexDirectoryPath);
                if (!f.exists() || !f.isDirectory()){
                    System.out.println("Folder '" + indexDirectoryPath + "' not exists or is not a directory");
                    System.exit(0);
                }
                
                indexer ind = new indexer(indexDirectoryPath);
                ind.indexSqLite(sqlLiteFilePath);
            }
            
            if(args[0].equals("suggester") || args[0].equals("build")){
                Suggester sug = new Suggester(sqlLiteFilePath);
                sug.buildParties();
            }
            
            if(args[0].equals("search")){
                searcher s = new searcher(args[2], sqlLiteFilePath);
                //System.out.println(s.search(args[3], 0, 10));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR");
            System.out.println(e.getMessage());
        }
        
       System.out.println("Indexer End");
        
    }
    
}
