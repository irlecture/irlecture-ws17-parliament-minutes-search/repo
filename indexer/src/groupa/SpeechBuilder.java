package groupa;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * We have to define a speech as a document,
 * i.e. a sequence whose parts/ items consists of speech parts, emotions, and chair statements.
 * Since the latter two can show up before a change of the speaker
 * a strict local approach for determining a speech is not possible.
 * Hence, a memory based approach is necessary.
 */
public class SpeechBuilder {

  public static class Item extends doc {
    public String itemType = "";
  }

  private LinkedList<Item> memory;

  // these are all the parameters which cannot be read from the first item, i.e. mostly the chair
  private String speakerOfPreviousSpeechPart;
  private String speakerNameOfPreviousSpeechPart;
  private String partyOfPreviousSpeechPart;
  private String topOfPreviousSpeechPart;
  private String speakerOfCurrentSpeechPart;
  private String speakerNameOfCurrentSpeechPart;
  private String partyOfCurrentSpeechPart;
  private String topOfCurrentSpeechPart;

  public SpeechBuilder() {
    memory = new LinkedList<>();
  }

  public void addItemAsResultSet(ResultSet rs) throws SQLException {
    Item item = new Item();
    item.id = rs.getInt("id");
    item.speaker = rs.getString("speaker");
    item.speaker_fp = rs.getString("speaker_fp");
    item.party = rs.getString("party");
    item.wp = rs.getInt("wp");
    item.s = rs.getInt("s");
    item.bTop = rs.getString("bTop");
    item.url = rs.getString("url");
    item.timestamp = Integer.parseInt(rs.getString("timestamp"));
    item.itemType = rs.getString("speech_type");
    item.emotions.add(rs.getString("emotion"));
    item.text = rs.getString("text");

    if (item.itemType.equals("speech")) {
      speakerOfPreviousSpeechPart = speakerOfCurrentSpeechPart;
      speakerNameOfPreviousSpeechPart = speakerNameOfCurrentSpeechPart;
      partyOfPreviousSpeechPart = partyOfCurrentSpeechPart;
      topOfPreviousSpeechPart = topOfCurrentSpeechPart;
      speakerOfCurrentSpeechPart = item.speaker_fp;
      speakerNameOfCurrentSpeechPart = item.speaker;
      partyOfCurrentSpeechPart = item.party;
      topOfCurrentSpeechPart = item.bTop;

    }
    memory.add(item);
  }

  public void addItem(Item item) {
    if (item.itemType.equals("speech")) {
      speakerOfPreviousSpeechPart = speakerOfCurrentSpeechPart;
      speakerNameOfPreviousSpeechPart = speakerNameOfCurrentSpeechPart;
      partyOfPreviousSpeechPart = partyOfCurrentSpeechPart;
      topOfPreviousSpeechPart = topOfCurrentSpeechPart;
      speakerOfCurrentSpeechPart = item.speaker_fp;
      speakerNameOfCurrentSpeechPart = item.speaker;
      partyOfCurrentSpeechPart = item.party;
      topOfCurrentSpeechPart = item.bTop;
    }
    memory.add(item);
  }

  public doc getNextSpeechAsDocument() {
    doc document = null;
    int lastItemPosition = checkEndOfSpeech();
    if (lastItemPosition >= 0) {
      document = buildSpeechAsDocument(lastItemPosition);
      deleteDocumentItemsFromMemory(lastItemPosition);
    }
    return  document;
  }

  public doc getLastSpeechAsDocument() {
    return buildSpeechAsDocument(memory.size() - 1);
  }

  private int checkEndOfSpeech() {
    if (speakerOfPreviousSpeechPart == null)
      return -1; // no item has been stored yet
    if (speakerOfPreviousSpeechPart.equals(speakerOfCurrentSpeechPart))
      return -1; // without a speaker change there is no next document
    else
      return positionOfLastItemOfPreviousSpeech();
  }

  private int positionOfLastItemOfPreviousSpeech() {
    // a speech document starts with
    // (chair (poi)*)? speech
    int positionOfLastItem = 0;
    int positionOfNewSpeaker = -42;
    for (int i = memory.size() - 1; i >= 0; i--) {
      Item consideredItem = memory.get(i);
      if (consideredItem.speaker_fp.equals(speakerOfCurrentSpeechPart)) {
        positionOfNewSpeaker = i;
        continue;
      }
      if (consideredItem.itemType.equals("chair")) {
        positionOfLastItem = i - 1;
        break;
      }
      if (consideredItem.speaker_fp.equals(speakerOfPreviousSpeechPart)) {
        positionOfLastItem = positionOfNewSpeaker - 1;
        break;
      }
    }
    return positionOfLastItem;
  }

  private doc buildSpeechAsDocument(int lastItemPosition) {
    doc document = initializeWithMetadata();
    StringBuilder textBuilder = new StringBuilder();
    for (int i = 0; i <= lastItemPosition; i++) {
      Item currentItem = memory.get(i);
      if (currentItem.itemType.equals("speech")) {
        textBuilder.append(currentItem.text);
        textBuilder.append(" ");
      } else if (currentItem.itemType.equals("poi")) {
        textBuilder.append("###E###");
        document.emotions.addAll(currentItem.emotions);
        document.emotionParties.add(currentItem.party);
        document.emotionTexts.add(currentItem.text);
      } else if (currentItem.itemType.equals("chair")) {
        textBuilder.append("###C###");
        document.chairTexts.add(currentItem.text);
        document.chairSpeaker.add(currentItem.speaker);
      }
    }
    document.text = textBuilder.toString();
    return document;
  }

  private doc initializeWithMetadata() {
    doc document = new doc();
    document.id = memory.get(0).id;
    document.speaker = speakerNameOfPreviousSpeechPart;
    document.speaker_fp = speakerOfPreviousSpeechPart;
    document.party = partyOfPreviousSpeechPart;
    document.wp = memory.get(0).wp;
    document.s = memory.get(0).s;
    document.bTop = topOfPreviousSpeechPart;
    document.url = memory.get(0).url;
    document.timestamp = memory.get(0).timestamp;

    return document;
  }

  private void deleteDocumentItemsFromMemory(int lastItemPosition) {
    for (int i = 0; i <= lastItemPosition; i++)
      memory.removeFirst();
    speakerOfPreviousSpeechPart = null;
  }

}
