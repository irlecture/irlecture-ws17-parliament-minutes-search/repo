import groupa.SpeechBuilder;
import groupa.SpeechBuilder.Item;
import groupa.doc;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpeechBuilderTest {

  @Test
  public void testFromDatabase() throws ClassNotFoundException, SQLException {
    List<doc> docs = docsForModifiedQuery(" AND speak.wp=19 AND speak.s=3 AND speak.sequence > 420 AND speak.sequence < 510 ");

    assertEquals(4, docs.size());
    assertEquals("stephan-mayer", docs.get(0).speaker_fp);
    assertEquals("gottfried-curio", docs.get(1).speaker_fp);
    assertEquals("stephan-thomae", docs.get(2).speaker_fp);
    assertEquals("zaklin-nastic", docs.get(3).speaker_fp);
    assertEquals("cducsu", docs.get(0).party);
    assertEquals("afd", docs.get(1).party);
    assertEquals("fdp", docs.get(2).party);
    assertEquals("linke", docs.get(3).party);
    for (int i = 0; i < 4; i++)
      assertEquals((long)19, (long)docs.get(i).wp);
    for (int i = 0; i < 4; i++)
      assertEquals((long)3, (long)docs.get(i).s);
    for (int i = 0; i < 4; i++)
      assertEquals("Antrag der Bundesregierung: Fortsetzung der Beteiligung bewaffneter deutscher Streitkräfte an der Multidimensionalen Integrierten Stabilisierungsmission der Vereinten Nationen in Mali (MINUSMA) auf Grundlage der Resolutionen 2100 (2013), 2164 (2014), 2227 (2015), 2295 (2016) und 2364 (2017) des Sicherheitsrates der Vereinten Nationen vom 25. April 2013, 25. Juni 2014, 29. Juni 2015, 29. Juni 2016 und 29. Juni 2017\n", docs.get(i).bTop);
    assertEquals(2, docs.get(0).emotionTexts.size());
    assertEquals(14, docs.get(1).emotionTexts.size());
    assertEquals(9, docs.get(2).emotionTexts.size());
    assertEquals(4, docs.get(3).emotionTexts.size());

    List<String> emotionTexts = docs.get(2).emotionTexts;
    assertEquals("Beifall bei der FDP und der SPD sowie bei Abgeordneten des BÜNDNISSES 90/DIE GRÜNEN", emotionTexts.get(emotionTexts.size() - 1));
  }

  @Test
  public void testBefragung() throws ClassNotFoundException, SQLException {
    List<doc> docs = docsForModifiedQuery(" AND speak.wp=18 AND speak.s=62 AND speak.sequence < 15 ");

    assertEquals(5, docs.size());
    assertEquals("alexander-dobrindt", docs.get(0).speaker_fp);
    assertEquals("valerie-wilms", docs.get(1).speaker_fp);
    assertEquals("alexander-dobrindt", docs.get(2).speaker_fp);
    assertEquals("matthias-gastel", docs.get(3).speaker_fp);
    assertEquals("alexander-dobrindt", docs.get(4).speaker_fp);
    assertEquals("", docs.get(0).party);
    assertEquals("gruene", docs.get(1).party);
    assertEquals("", docs.get(2).party);
    assertEquals("gruene", docs.get(3).party);
    assertEquals("", docs.get(4).party);
    for (int i = 0; i < 5; i++)
      assertEquals((long)18, (long)docs.get(i).wp);
    for (int i = 0; i < 5; i++)
      assertEquals((long)62, (long)docs.get(i).s);
    for (int i = 0; i < 5; i++)
      assertTrue(docs.get(i).bTop.startsWith("Befragung der Bundesregierung: Entwurf -eines Dritten Gesetzes zur Änderung des Bundesfernstraßenmautgesetzes - Aus-weitung der Lkw-Maut"));
    for (int i = 0; i < 5; i++)
      assertEquals(1, docs.get(i).chairTexts.size());
    for (int i = 0; i < 5; i++)
      assertEquals(0, docs.get(i).emotions.size());
  }

  @Test
  public void testTopicChange() throws SQLException, ClassNotFoundException {
    List<doc> docs = docsForModifiedQuery(" AND speak.wp = 18 AND speak.s = 30 AND speak.sequence > 940 AND speak.sequence < 950 ");

    assertEquals(2, docs.size());
    assertEquals("anette-hubinger", docs.get(0).speaker_fp);
    assertEquals("manuela-schwesig", docs.get(1).speaker_fp);
    assertEquals("cducsu", docs.get(0).party);
    assertEquals("", docs.get(1).party);
    for (int i = 0; i < 2; i++)
      assertEquals((long)18, (long)docs.get(i).wp);
    for (int i = 0; i < 2; i++)
      assertEquals((long)30, (long)docs.get(i).s);
    assertTrue(docs.get(0).bTop.startsWith("a)\tErste Beratung des von der Bundesregierung eingebrachten Entwurfs eines Gesetzes über die Feststellung des Bundeshaushaltsplans für das Haushaltsjahr 2014 (Haushaltsgesetz 2014)\n"));
    assertTrue(docs.get(1).bTop.startsWith("a)\tAntrag des Bundesministeriums der Finanzen: Entlastung der Bundesregierung für das Haushaltsjahr 2012: - Vorlage der Haushaltsrechnung des Bundes für das Haushaltsjahr 2012 -\n"));
    assertEquals(0, docs.get(0).chairTexts.size());
    assertEquals(1, docs.get(1).chairTexts.size());
    assertEquals(2, docs.get(0).emotions.size());
    assertEquals(3, docs.get(1).emotions.size());
  }

  @Test
  public void simulateSpeechBuilder() {
    SpeechBuilder speechBuilder = new SpeechBuilder();
    ArrayList<doc> docs = new ArrayList<>();
    doc currentDocument = new doc();
    for(Item item : data()) {
      speechBuilder.addItem(item);
      currentDocument = speechBuilder.getNextSpeechAsDocument();
      if (currentDocument != null)
        docs.add(currentDocument);
    }
    docs.add(speechBuilder.getLastSpeechAsDocument());

    assertEquals(2, docs.size());

    assertEquals((long)0, (long)docs.get(0).id);
    assertEquals("politician_0", docs.get(0).speaker_fp);
    assertEquals("that's sentence 0. ###C######E###that's sentence 3 from same politician as 0. ###E###", docs.get(0).text);
    assertEquals("that's poi 4.", docs.get(0).emotionTexts.get(1));

    assertEquals((long)5, (long)docs.get(1).id);
    assertEquals("politician_1", docs.get(1).speaker_fp);
    assertEquals("###C######E###that's sentence 7 from a new politician. ###E###that's sentence 9 from the new politician. ", docs.get(1).text);
    assertEquals("that's poi 8.", docs.get(1).emotionTexts.get(1));
  }

  private List<Item> data() {
    List<Item> data = new ArrayList<>();
    
    Item item0 = new Item();
    item0.id = 0;
    item0.speaker = "politician 0";
    item0.speaker_fp = "politician_0";
    item0.party = "party P";
    item0.wp = 21;
    item0.s = 42;
    item0.bTop = "topic T";
    item0.itemType = "speech";
    item0.text = "that's sentence 0.";
    data.add(item0);
    
    Item item1 = new Item();
    item1.id = 1;
    item1.speaker_fp = "chair_speaker";
    item1.itemType = "chair";
    item1.text = "that's chair 1.";
    data.add(item1);
    
    Item item2 = new Item();
    item2.id = 2;
    item2.itemType = "poi";
    item2.text = "that's poi 2.";
    data.add(item2);

    Item item3 = new Item();
    item3.id = 3;
    item3.speaker_fp = "politician_0";
    item3.itemType = "speech";
    item3.text = "that's sentence 3 from same politician as 0.";
    data.add(item3);

    Item item4 = new Item();
    item4.id = 4;
    item4.itemType = "poi";
    item4.text = "that's poi 4.";
    data.add(item4);

    Item item5 = new Item();
    item5.id = 5;
    item4.speaker_fp = "chair_speaker";
    item5.itemType = "chair";
    item5.text = "that's chair 5.";
    data.add(item5);

    Item item6 = new Item();
    item6.id = 6;
    item6.itemType = "poi";
    item6.text = "that's poi 6.";
    data.add(item6);

    Item item7 = new Item();
    item7.id = 7;
    item7.speaker_fp = "politician_1";
    item7.itemType = "speech";
    item7.text = "that's sentence 7 from a new politician.";
    data.add(item7);

    Item item8 = new Item();
    item8.id = 8;
    item8.itemType = "poi";
    item8.text = "that's poi 8.";
    data.add(item8);

    Item item9 = new Item();
    item9.id = 9;
    item9.speaker_fp = "politician_1";
    item9.itemType = "speech";
    item9.text = "that's sentence 9 from the new politician.";
    data.add(item9);

    return data;
  }

  private List<doc> docsForModifiedQuery(String modification) throws ClassNotFoundException, SQLException {
    String sqlFileFilePath = "../data/db/db.sqlite";
    Class.forName("org.sqlite.JDBC");
    Connection c = DriverManager.getConnection("jdbc:sqlite:" + sqlFileFilePath);

    Statement stmt = c.createStatement();
    ResultSet rs = stmt.executeQuery(queryForIndexerDataModified(modification));
    ArrayList<doc> docs = new ArrayList<>();
    SpeechBuilder speechBuilder = new SpeechBuilder();
    doc currentDocument = new doc();
    while ( rs.next() ) {
      speechBuilder.addItemAsResultSet(rs);
      currentDocument = speechBuilder.getNextSpeechAsDocument();
      if (currentDocument != null)
        docs.add(currentDocument);
    }
    docs.add(speechBuilder.getLastSpeechAsDocument());
    return docs;
  }

  private String queryForIndexerDataModified(String modification) {
    return "SELECT speak.id, "
        + "     IFNULL(speak.speaker, '') AS speaker,"
        + "     IFNULL(speak.speaker_fp, '') AS speaker_fp,"
        + "     IFNULL(speak.text, '') as text,"
        + "     IFNULL(speak.speaker_party, '') as party,"
        + "     IFNULL(speak.emotion, '') as emotion,"
        + "     IFNULL(speak.type, '') as speech_type,"
        + "     speak.wp,"
        + "     speak.s,"
        + "     IFNULL(tops.topic, '') as bTop,"
        + "     IFNULL(protocol.url,'') as url,"
        + "     strftime('%s', IFNULL(protocol.date, 'now')) as timestamp"
        + " FROM speak"
        + " LEFT JOIN tops"
        + "     ON tops.id = speak.top_id"
        + " LEFT JOIN protocol"
        + "     ON protocol.wp = speak.wp"
        + "     AND protocol.s = speak.s"
        + " WHERE (speak.type = 'speech'"
        + "    OR ( speak.type = 'poi' AND emotion IS NOT NULL )"
        + "    OR speak.type = 'chair')"
        + modification
        + " ORDER BY speak.wp, speak.s, speak.sequence;";
  }
}
